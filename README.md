# Efektivita omezujících opatření zavedených proti šíření koronaviru

Tato analýza se věnuje efektivitě omezujících opatření zavedené v jednotlivých zemích. Především by pak tato analýza měla zodpovědět tyto otázky:

1.   Kdy začali mít účinnost nařízená opatření v dané zemi?
2.   Jak byly efektivní nařízená opatření v dané zemi?
3.   Díky kterým nařízením se docílilo tohoto poklesu v dané zemi?

Jako dataset pro analýzu je použit OxCGRT ([The Oxford Covid-19 Government Response Tracker](https://github.com/OxCGRT/covid-policy-tracker)), ve kterém jsou denní informace zemí o pacientech (počet nakažených, ...), o nařízeních vlády a kdy byly provedeny.